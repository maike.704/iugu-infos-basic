## Auth
* Todas as chamadas são feitas com Basic Auth;
* O token deve ser gerado no dashboard e passado no cabeçalho da requisição, concatenado com ":" (dois pontos) e codificado em base64;

### Cliente
1. Criar Cliente (nome, email);
    * cpf_cnpj obrigatório apenas para emissão de boletos;
    * campos de endereço opcional caso zip_code = null;
2. Adicionar forma pgto (client id, token, desc);
    * se atentar pois o front terá que consumir uma lib do iugu, pra gerar o token dos cartões de cred, as informações cruas
 de cartão do cliente não são transportadas via http(s);
    * essa mesma lib de geração de tokens dos cartões de crédito não suportava cartões temporários, não constava essa
limitação na documentação, não sei se persiste essa limitação;

Cliente já pode fazer pedidos...

## MarketPlace
### Subconta
1. Criar subconta (nome, e regras de comissão do administrador);
    * uma vez criado a subconta irá retornar os tokens para transação, caso os tokens sejam perdidos eles não pode mais
    ser recuperados;
2. Solicitação de validação da subconta;
    * Nesse passo é informado a conta bancária para realização do split, eles dizem ter altos recritérios, mas baseados
    no que vimos na myselfpay, oq eles enchem o saco é só o nome e num de documento, caso PF, ou, cnpj, nome responsável
    e cpf responsável, caso PJ;
3. Conta deve ser aprovada em até 24h;
    * Se atentar pois o serviço do iugu é péssimo em caso de reprovações, as vezes, por algum motivo, quando a conta é reprovada
    eles não atualizavam na hora o status, a conta ficava cozinhando pendente de aprovação e só tinhamos uma posição qndo
    entravamos em contato com o suporte;

Conta do market place já pode receber pedidos...

### Hooks
* Talvez será necessário criação de eventos para assegurar que os valores sejam repassados apenas apoós processados e disponíveis;
    * Por que talvez? Por algum motivo, mesmo o iugu já ter opções de comissionamento, que, creio eu, deve ser usado no
    momento do split, no momento que foi feito a integração no MSP, todas as transações eram feitas para conta MASTER,
    era feito um gancho no evento invoice.status_changed e verificado o recebmento do pagamento (note que faturar != receber),
    e depois transferiamos o valor através do endpoint de transferência, com a comição deduzida. Não sei será o caso de
    uso de vcs, se os caras cagaram na hr de integrar, se o iugu fazia o split assim que o pgto era faturado e dava algum
    b.o em caso de chargeback;
